// app.js

// define our application and pull in ngRoute and ngAnimate
var pauApp = angular.module('animateApp', ['ngRoute', 'ngAnimate', 'ngMaterial']);

// ROUTING ===============================================
// set our routing for this application
// each route will pull in a different controller
pauApp.config(function($routeProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'components/home/homeView.html',
            controller: 'homeController'
        })
        
        // about page
        .when('/about', {
            templateUrl: 'components/about/page-about.html',
            controller: 'aboutController'
        })

     

});

